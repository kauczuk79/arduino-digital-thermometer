#include <LiquidCrystal.h>
#include <OneWire.h>
#include <DS18B20.h>

#define ONE_WIRE_BUS 10
#define RS 12
#define EN 11
#define D4 5
#define D5 4
#define D6 3
#define D7 2

OneWire oneWire(ONE_WIRE_BUS);
DS18B20 sensor(&oneWire);

LiquidCrystal lcd(RS, EN, D4, D5, D6, D7);

PROGMEM const char tempMessage[] = {"Temperature: "};
PROGMEM const char celsius[] = {" C"};

void setup() {
  lcd.begin(16, 2);
  sensor.begin();
  sensor.setResolution(12);
}

void loop() {
  lcd.setCursor(0,0);
  lcd.print(tempMessage);
  lcd.setCursor(0, 1);
  sensor.requestTemperatures();
  delay(500);
  lcd.print(sensor.getTempC());
  lcd.print(celsius);
}
