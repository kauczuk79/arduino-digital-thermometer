# arduino-digital-thermometer

Thermometer based on DS18B20 digital sensor displaing temperature in celsius scale on HD44780 2x16 display.

## Software

- Arduino IDE 1.8.6

## Required parts

- Any Arduino board (tested with UNO R3 and Leonardo)
- HD44780 display
- DS18B20 sensor
- 4.7kOhm resistor (pull up resistor for data pin in the sensor)
- 10k linear potentiometer (for setup contrast in HD44780)

## Libraries

- OneWire (https://github.com/PaulStoffregen/OneWire, version 2.3.4)
- DS18B20 (https://github.com/RobTillaart/Arduino.git, version 0.1.0)
- LiquidCrystal (version 1.0.7)